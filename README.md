# JFSMA2024

Ressources pour les [32èmes Journées Francophones sur les Systèmes Multi-Agents (JFSMA 2024)](https://easychair.org/smart-program/JFSMA2024/)

## Instructions aux auteurs

Les templates LaTeX et Word à respecter pour la soumission et la version finale des articles sont disponibles [ici](https://forgemia.inra.fr/spicault/jfsma2024/-/raw/main/Styles_JFSMA24.zip).
